package com.hobbitapi;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dwarf")
public class HobbitController {

    @PersistenceContext
    private EntityManager entityManager;


    @PostMapping("/{nickname}")
    public ResponseEntity<Hobbit> createHobbit(@RequestBody Hobbit hobbit, @PathVariable("nickname") String nickname) {
        Hobbit savedHobbit = save(hobbit);
        return ResponseEntity.ok(savedHobbit);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Hobbit> getHobbit(@PathVariable Long id) {
        // Business logic in controller
        Hobbit hobbit = findById(id);
        if (hobbit == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(hobbit);
    }

    public Hobbit save(Hobbit hobbit) {
        entityManager.persist(hobbit);
        return hobbit;
    }

    public Hobbit findById(Long id) {
        return entityManager.find(Hobbit.class, id);
    }

}
